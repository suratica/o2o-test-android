# Endesa-app


npm install @ionic/app-scripts@latest --save-dev

#Dependencias y plugins para que el proyecto funcione:
##Chart.js
https://www.chartjs.org/docs/latest/getting-started/installation.html

npm install chart.js --save
##Circle-progress-bar
https://www.npmjs.com/package/ng-circle-progress

npm install ng-circle-progress@1.0.0 --save
##AutoStart
https://ionicframework.com/docs/v3/native/autostart/

ionic cordova plugin add cordova-plugin-autostart && npm install --save @ionic-native/autostart@4
##Background-mode
https://ionicframework.com/docs/v3/native/background-mode/
ionic cordova plugin add cordova-plugin-background-mode && npm install --save @ionic-native/background-mode@4

Modificar el BackgroundMode.java para que no pare al destruir la aplicación.
##Moment
npm install moment --save

https://momentjs.com/
##Pedometer
https://ionicframework.com/docs/v3/native/pedometer/

ionic cordova plugin add cordova-plugin-pedometer && npm install --save @ionic-native/pedometer@4
##SQLITE
https://blog.ng-classroom.com/blog/ionic2/sqlite-and-ionic/

https://ionicframework.com/docs/v3/native/sqlite/

ionic cordova plugin add cordova-sqlite-storage && npm install --save @ionic-native/sqlite@4
##Stepcounter
https://ionicframework.com/docs/v3/native/stepcounter/

ionic cordova plugin add cordova-plugin-stepcounter && npm install --save @ionic-native/stepcounter@4
##Storage
https://ionicframework.com/docs/v3/storage/

ionic cordova plugin add cordova-sqlite-storage && npm install --save @ionic/storage
    
##Customer plugin
ionic cordova plugin rm cordova-plugin-helloworld

ionic cordova plugin add plugins_src/HelloWorld/

crear provider para el plugin
####OPTIONAL
npm install --save @ionic-native/device-motion@4
